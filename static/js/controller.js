var app = angular.module("myApp", []);

var host = "127.0.0.1:8080";

app.controller("MainController", function ($scope) {
  $scope.recieve_message = "";
  $scope.log = [];
  $scope.message = "";
  $scope.number_max = 100;
  $scope.number_min = 1;
  $scope.number = $scope.number_min;
  $scope.error = false;
  $scope.player = "Unknown";
  
  $scope.put_message = function(message) {
    message = "<" + (new Date()).toLocaleTimeString() + "> " + message;
    $scope.log.push(message);
    $scope.recieve_message = message;
  }
  $scope.send_message = function($event) {
    $event.preventDefault();
    if($scope.message != "")
      $scope.web_socket.JSON_send({"message": $scope.message});
    $scope.message = "";
  }
  $scope.send_number = function($event) {
    $event.preventDefault();
    if($scope.number_min <= $scope.number && $scope.number <= $scope.number_max){
      $scope.web_socket.JSON_send({"number": $scope.number});
      $scope.number = $scope.number_min;
      $scope.error = false;
    } else {
      $scope.error = true;
    }
  }
  
  $scope.put_message("Connecting to Server...");
  
  $scope.web_socket = new WebSocket("wss://" + host);
  $scope.web_socket.onopen = function() {$scope.$apply($scope.put_message("Connected to Server."));};
  $scope.web_socket.onmessage = function(web_socket) {
    var data = JSON.parse(web_socket.data);
    if(typeof data.message == "string"){
      $scope.$apply($scope.put_message(data.message));
    }
    if(typeof data.max == "number") {
      $scope.$apply($scope.number_max = data.max);
      if($scope.number > $scope.number_max) $scope.$apply($scope.number = $scope.number_max);
    }
    if(typeof data.min == "number") {
      $scope.$apply($scope.number_min = data.min);
      if($scope.number < $scope.number_min) $scope.$apply($scope.number = $scope.number_min);
    }
    if(typeof data.player == "number") $scope.$apply($scope.player = data.player);
  };
  $scope.web_socket.onclose  = function() {$scope.$apply($scope.put_message("Disconnected from Server."));};
  $scope.web_socket.JSON_send = function(message) {$scope.web_socket.send(JSON.stringify(message))};
});
