//Import
var https = require('https');
var fs = require('fs');
var url = require('url');
var WebSocketServer = require('ws').Server;
var WebSocket = require('ws');

//Config
var STATIC_DIR = "./static";
var ROOM_SIZE = 3;
var NUMBER_MAX = 100;
var NUMBER_MIN = 1;
var SYSTEM_BROCAST_HEADER = "== Lobby ==: ";

//Declaration
var game_manager = new Object;
var lobby_manager = new Object;

//WebSocket Data Handlers
function message_handler(data, web_socket, web_socket_server) {
  web_socket_server.clients.forEach(function do_task(client){
    if(typeof data.message == "string")
      client.JSON_send_messgae("Player " + web_socket.game.id  + ": " + data.message);
  })
}
function number_handler(data, web_socket, web_socket_server) {
  if(game_manager.whos_turn(web_socket_server) != web_socket.game.id) return;
  var parse_number = parseInt(data.number);
  var guess = false;
  if(!isNaN(parse_number) && game_manager.legal_guess(parse_number)) {
    guess = game_manager.guess(parse_number);
    web_socket_server.clients.forEach(function do_task(client){    
      client.JSON_send_messgae("Player " + web_socket.game.id  + " Chooce: " + parse_number);
      if(guess) {
        client.lobby_send("!!!!!!BOOM!BOOM!BOOM!!!!!!");
        client.lobby_send("Reset the game.");
      }
      client.JSON_send(game_manager.range());
      client.lobby_send("Current range: " + game_manager.game.min + " ~ " + game_manager.game.max);
      client.lobby_send("It's player " + game_manager.whos_turn(web_socket_server) + "'s turn.");
    })
  }
}
var current_handlers = {"message": message_handler};
  
//Game Manager services
game_manager.started = false;
game_manager.turn = 0;
game_manager.start_game = function start_game() {
  game_manager.started = true;
  game_manager.game = {
    "max": NUMBER_MAX, "min": NUMBER_MIN,
    "target": Math.round(Math.random()*(NUMBER_MAX-1) + NUMBER_MIN)};
  current_handlers.number = number_handler;
}
game_manager.stop_game = function stop_game() {
  game_manager.started = false;
  delete current_handlers.number;
}
game_manager.check = function check(web_socket_server) {
  if(web_socket_server.clients.length < ROOM_SIZE) return false;
  game_manager.start_game();
  return true;
}
game_manager.guess = function guess(number) {
  game_manager.turn = (game_manager.turn + 1) % ROOM_SIZE;
  if(number == game_manager.game.target) {
    game_manager.start_game();
    return true;
  }
  if(number > game_manager.game.target) game_manager.game.max = number - 1;
  else game_manager.game.min = number + 1;
  return false;
}
game_manager.legal_guess = function legal_guess(number) {
  return game_manager.game.max >= number && number >= game_manager.game.min;
}
game_manager.range = function range(number) {
  return {"max": game_manager.game.max, "min": game_manager.game.min};
}
game_manager.whos_turn = function range(web_socket_server) {
  return web_socket_server.clients[game_manager.turn].game.id;
}

//Lobby Manager services
lobby_manager.next_client_id = 0;
lobby_manager.join = function join(web_socket, web_socket_server){
  if(web_socket_server.clients.length <= ROOM_SIZE) {
    web_socket.game = {id: lobby_manager.next_client_id};
    lobby_manager.next_client_id++;
    web_socket.lobby_send("Welcome to the game!");
    web_socket.JSON_send({"player": web_socket.game.id});
    var started = game_manager.check(web_socket_server);
    web_socket_server.clients.forEach(function do_task(client){
      client.lobby_send("Player " + web_socket.game.id  +" joined the game.");
      if(started) {
        client.lobby_send("The game started!");
        client.JSON_send(game_manager.range());
        client.lobby_send("Current range: " + game_manager.game.min + " ~ " + game_manager.game.max);
        client.lobby_send("It's player " + game_manager.whos_turn(web_socket_server) + "'s turn.");
      }
      else client.lobby_send("The game needs another " + (ROOM_SIZE - web_socket_server.clients.length) + " Player(s).");
    });
    return true;
  } else {
    web_socket.lobby_send("Server is full.");
    web_socket.close();
    return false;  
  }
}
lobby_manager.close = function close_lobby(web_socket_server){
  web_socket_server.clients.forEach(function do_task(client){
    client.lobby_send("Lobby Closed.");
    client.close();
  })
}

//Utilities
function extent_web_socket(web_socket) {
  web_socket.JSON_send = function(message_object){
    if(web_socket.readyState == WebSocket.OPEN) web_socket.send(JSON.stringify(message_object))
  }
  web_socket.JSON_send_messgae = function(message){web_socket.JSON_send({"message": message})}
  web_socket.lobby_send = function(message){web_socket.JSON_send_messgae(SYSTEM_BROCAST_HEADER + message)};
}
function web_socket_config_callbacks(web_socket, web_socket_server) {
  web_socket.on("message", function message_handler(message){
    var data = JSON.parse(message);
    for(var index in current_handlers)current_handlers[index](data, web_socket, web_socket_server);
  })
  web_socket.on("close", function message_handler(){
    var canceled = false;
    if(game_manager.started){
      game_manager.stop_game();
      canceled = true;
    }
    web_socket_server.clients.forEach(function do_task(client){
      client.lobby_send("Player " + web_socket.game.id  + " leaved the game.");
      client.lobby_send("The game needs another " + (ROOM_SIZE - web_socket_server.clients.length) + " Player(s).");
      if(canceled) client.lobby_send("The game canceled!");
    })
  })
}

//Https Server Setup
var https_server = https.createServer({
  "key": fs.readFileSync("./server.key"),
  "cert": fs.readFileSync("./server.crt")
}, function http_server_handler(request, response) {
  var pathname = url.parse(request.url).pathname;
  if(pathname == "/") pathname = "/index.html";
  pathname = STATIC_DIR + pathname;
  fs.lstat(pathname , function if_exist_do(error, status) {
    if(!error && status.isFile()) {
      response.writeHead(200);
      response.end(fs.readFileSync(pathname));
    } else {
      response.writeHead(404);
      response.end("Not Found");
    }
  })
});

//HTTPS Server Listen Setup & Web Socket Server Setup
https_server.listen(8080, function after_create() {
  var web_socket_server = new WebSocketServer({"server": https_server});
  
  web_socket_server.on('connection', function connection_handler(web_socket) {
    extent_web_socket(web_socket);
    if(lobby_manager.join(web_socket, web_socket_server)) web_socket_config_callbacks(web_socket, web_socket_server);
  });
});
